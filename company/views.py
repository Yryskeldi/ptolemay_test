from django.db.models import Sum, Count
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, AllowAny

from PtolemayTest.paginations import CustomPagination
from .models import Department, Employee
from .serializers import DepartmentSerializer, EmployeeSerializer


class EmployeeListAPIView(generics.ListAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = [IsAuthenticated]
    filterset_fields = ['department__id', 'full_name']
    pagination_class = CustomPagination

    def get_queryset(self):
        queryset = super().get_queryset()
        department_id = self.request.query_params.get('department_id')
        full_name = self.request.query_params.get('full_name')
        if department_id:
            queryset = queryset.filter(department_id=department_id)
        if full_name:
            queryset = queryset.filter(full_name__icontains=full_name)
        return queryset


class EmployeeCreateAPIView(generics.CreateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = [IsAuthenticated]


class DepartmentListAPIView(generics.ListAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.annotate(
            num_employees=Count('employees'),
            total_salary=Sum('employees__salary')
        )
        return queryset
