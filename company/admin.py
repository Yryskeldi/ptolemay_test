from django.contrib import admin
from .models import Department, Employee


class EmployeeInline(admin.TabularInline):
    model = Employee
    extra = 1


class DepartmentAdmin(admin.ModelAdmin):
    inlines = (EmployeeInline,)


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'position', 'department')
    list_filter = ('department',)
    search_fields = ('full_name',)


admin.site.register(Department, DepartmentAdmin)
admin.site.register(Employee, EmployeeAdmin)
