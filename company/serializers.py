from django.db.models import Sum
from rest_framework import serializers
from .models import Department, Employee


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = '__all__'


class DepartmentSerializer(serializers.ModelSerializer):
    num_employees = serializers.SerializerMethodField()
    total_salary = serializers.SerializerMethodField()

    class Meta:
        model = Department
        fields = ['id', 'name', 'num_employees', 'total_salary']

    def get_num_employees(self, obj):
        return obj.employees.count()

    def get_total_salary(self, obj):
        return obj.employees.all().aggregate(total_salary=Sum('salary'))['total_salary']
