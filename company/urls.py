from django.urls import path
from company import views

urlpatterns = [
    path('departments/', views.DepartmentListAPIView.as_view(), name='department-list'),
    path('departments/<int:pk>/', views.DepartmentDetailAPIView.as_view(), name='department-detail'),
    path('employees/', views.EmployeeListAPIView.as_view(), name='employee-list'),
    path('employees/<int:pk>/', views.EmployeeDetailAPIView.as_view(), name='employee-detail'),
]