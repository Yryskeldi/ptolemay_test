from django.db import models


class Department(models.Model):
    class Meta:
        verbose_name = 'Отдел'
        verbose_name_plural = 'Отделы'

    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Employee(models.Model):
    class Meta:
        unique_together = ('id', 'department')
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    full_name = models.CharField(max_length=255, db_index=True)
    photo = models.ImageField(upload_to='employee_photos', blank=True, null=True)
    position = models.CharField(max_length=255)
    salary = models.DecimalField(max_digits=10, decimal_places=2)
    age = models.PositiveIntegerField()
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='employees')

    def __str__(self):
        return self.full_name
