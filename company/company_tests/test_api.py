import json

from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from company.company_tests.factories import DepartmentFactory, EmployeeFactory



from company.models import Department, Employee


class EmployeeAPITest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_get_employee_list(self):
        EmployeeFactory.create_batch(5)
        url = reverse('employee-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(json.loads(response.content))

    def test_filter_employee_by_department(self):
        department = DepartmentFactory()
        EmployeeFactory.create_batch(3, department=department)
        EmployeeFactory.create_batch(2)
        url = reverse('employee-list') + f'?department_id={department.id}'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_filter_employee_by_full_name(self):
        EmployeeFactory.create_batch(15)
        url = reverse('employee-list') + '?full_name=Kate'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_employee(self):
        department = DepartmentFactory()
        employee_data = {
            'department': department.id,
            'full_name': 'John',
            'position': 'Manager',
            'salary': 5000,
            'age': 35
        }
        response = self.client.post(reverse('employee-create'), employee_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Employee.objects.count(), 1)


class DepartmentAPITest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_get_department_list(self):
        DepartmentFactory.create_batch(1)
        EmployeeFactory.create_batch(10, department=Department.objects.first())
        response = self.client.get(reverse('department-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
