import factory
from faker import Faker
from company.models import Department, Employee

fake = Faker()


class DepartmentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Department

    name = factory.Sequence(lambda n: f"Department {n}")


class EmployeeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Employee

    department = factory.SubFactory(DepartmentFactory)
    full_name = fake.first_name()
    position = fake.job()
    salary = factory.Faker('pyint', min_value=3000, max_value=8000)
    age = factory.Faker('pyint', min_value=22, max_value=60)
