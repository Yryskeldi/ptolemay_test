from rest_framework.pagination import PageNumberPagination


class CustomPagination(PageNumberPagination):
    page_size = 10  # Number of items to include in each page
    page_size_query_param = 'page_size'  # Custom query parameter to specify the page size
    max_page_size = 100  # Maximum allowed page size
